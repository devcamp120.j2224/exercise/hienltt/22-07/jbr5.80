package com.devcamp.music_api.model;

public class Composer extends Person{
    private String stagename;

    public Composer(String firstname, String lastname) {
        super(firstname, lastname);
        //TODO Auto-generated constructor stub
    }

    public Composer(String firstname, String lastname, String stagename) {
        super(firstname, lastname);
        this.stagename = stagename;
    }

    public String getStagename() {
        return stagename;
    }

    public void setStagename(String stagename) {
        this.stagename = stagename;
    }    
}
